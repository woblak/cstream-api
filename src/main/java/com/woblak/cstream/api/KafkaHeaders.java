package com.woblak.cstream.api;

public abstract class KafkaHeaders extends org.springframework.kafka.support.KafkaHeaders {

    public static final String EVENT = "event";
    public static final String VERSION = "version";
    public static final String LAST_TIMESTAMP = "lastTimestamp";
}