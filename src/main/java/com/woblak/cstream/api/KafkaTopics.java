package com.woblak.cstream.api;

public abstract class KafkaTopics {

    public static final String CRYPTO_TRADES = "crypto-trades";
    public static final String CRYPTOWAT_TRADES = "cryptowat-trades";
    public static final String CRYPTO_AGG_MEAN = "crypto-agg-mean";
    public static final String CRYPTO_AGG_SUMMARY = "crypto-agg-summary";
    public static final String SSE_NOTIFICATIONS = "sse-notifications";
}
